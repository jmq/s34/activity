const express = require('express')
const router = express.Router()
const courseController = require('../controllers/courseControllers')
const auth = require('../auth')

//Route for creating a course
router.post('/', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	
	if(userData.isAdmin) {
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(false)
	}
})

module.exports = router

//Routes for user details
// router.get('/details', auth.verify, (req, res) => {
// 	const userData = auth.decode(req.headers.authorization)
// 	console.log(userData)

// 	userController.getProfile({userId: userData.id}).then(controllerRes => res.send(controllerRes))

// 	//userController.getUserDetails(req.body).then(controllerRes => res.send(controllerRes))
// })