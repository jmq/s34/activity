const express = require('express')
const userController = require('../controllers/userController')
const app = express()
const router = express.Router() //Allows access to HTTP method middlewares to create routes
const auth = require('../auth')


//Route for User registration
router.post('/register', (req, res) => {
	userController.registerUser(req.body).then(controllerRes => res.send(controllerRes))
})

//Route to GET all User
router.get('/', (req, res) => {
	userController.getUsers().then(controllerRes => res.send(controllerRes))
})

//Check email if already exists
router.post('/checkEmail', (req, res) => {
	userController.checkEmailExists(req.body).then(controllerRes => res.send(controllerRes))
})

//Routes for user authentication
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(controllerRes => res.send(controllerRes))
})


//Routes for user details
router.get('/details', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	console.log(userData)

	userController.getProfile({userId: userData.id}).then(controllerRes => res.send(controllerRes))

	//userController.getUserDetails(req.body).then(controllerRes => res.send(controllerRes))
})


module.exports = router



// // Route t get all the tasks
// router.get('/', (req, res) => {
// 	taskController.getAllTasks().then(resultFromController => {
// 		res.send(resultFromController)
// 	})
// })

// // Route to create a task
// // .then sends the result to client or postman
// router.post('/', (req, res) => {
// 	taskController.createTask(req.body).then(resultFromController => {
// 		res.send(resultFromController)
// 	})
// })


// // Route to Delete
// router.delete('/:id', (req, res) => {
// 	console.log(req.params)
// 	taskController.deleteTask(req.params.id).then(resultFromController => {
// 		res.send(`Task ${resultFromController.name} has been deleted.`)
// 	})
// })


// // Route to update
// router.put('/:id', (req, res) => {
// 	taskController.updateTask(req.params.id, req.body).then(resultFromController => {
// 		res.send(`${req.params.id} has been updated ${resultFromController.status}.`)
// 	})
// })


// // Route to get specific task
// router.get('/:id', (req, res) => {
// 	taskController.getSpecificTask(req.params.id).then(resultFromController => {
// 		res.send(resultFromController)
// 	})
// })

// // Route to update specific task
// router.put('/:id/complete', (req, res) => {
// 	taskController.completeTask(req.params.id).then(resultFromController => {
// 		res.send(resultFromController)
// 	})
// })