const jwt = require('jsonwebtoken')
const secret = 'CourseBookingAPI'

//Create jsonWebToken
module.exports.createAccessToken = (user) => { //user variable accepts req.body
	//When the user login, the token will be created 
	//with the user's information
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
	//Generate JSON web token using sign method
	// sign method - generates the token using form data
	//               and the secret code with 
	//               no additional options provided
	return jwt.sign(data, secret, {})
}

// Verify
module.exports.verify = (req, res, next) => {
	let token = req.headers.authorization
	if(typeof token !== 'undefined') {
		console.log(token)

		token = token.slice(7, token.length)

		return jwt.verify(token, secret, (err, data) => {
			if(err) {
				return res.send({auth: 'failed'})
			} else {
				next()
			}
		})
	} else {
		return res.send({auth: 'failed'})
	}
}

// Decode
module.exports.decode = (token) => {
	if(typeof token !== 'undefined') {
		token = token.slice(7, token.length)
		return jwt.verify(token, secret, (err, data) => {
			if(err) {
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload
			}
		})
	} else {
		return null
	}
}