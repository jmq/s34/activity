const user = require('../models/user')
const bcrypt = require('bcrypt')
const auth = require('../auth')


//Get all users
module.exports.getUsers = () => {
	return user.find({}).then(result => {
		return result
	})
}

//User Regsitration
module.exports.registerUser = (requestBody) => {
	const newUser = new user({
		firstName: requestBody.firstName,
		lastName: requestBody.lastName,
		email: requestBody.email,
		password: bcrypt.hashSync(requestBody.password, 10), //salt rounds:10
		isAdmin: requestBody.isAdmin,
		mobileNo: requestBody.mobileNo,
		enrollments: requestBody.enrollments
	})

	return newUser.save().then((user, error) => {
		if(error) {
			console.log(error)
			return false
		} else {
			return true
		}
	})
}

//checkEmailExists check
module.exports.checkEmailExists = (reqBody) => {
	return user.find({email: reqBody.email}).then(result => {
		return (result.length > 0) ? true : false
	})
}


//User Authentication
module.exports.loginUser = (reqBody) => {
	return user.findOne({email: reqBody.email}).then(result => {
		if(result === null) {
			return false
		} else {
			//compareSync(dataToBeCompared, enrcyptedData), boolean result
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			if(isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

//getUserDetails
module.exports.getUserDetails = (reqBody) => {
	return user.findOne({id: reqBody._id}).then(result => {
		if(result === null) {
			return false
		} else {
			result.password = ''
			return result
		}
	})
}


module.exports.getProfile = (data) => {
	return user.findById(data.userId).then(result => {
		result.password = ''
		return result		
	})
}

// Controller for updating a specific task

// module.exports.completeTask = (taskID) => {
// 	//return Task.findByIdAndUpdate(task)
// 	return Task.findById(taskID).then((result, error) => {
// 		if(error) {
// 			console.log(error)
// 			return false
// 		} 
// 		result.status = 'complete'
// 		return result.save().then((updatedTask, saveErr) => {
// 			if(saveErr) {
// 				console.log(saveErr)
// 				return false
// 			} else {
// 				return updatedTask
// 			}
// 		})		
// 	})
// }
