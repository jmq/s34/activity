const Course = require('../models/course')

module.exports.addCourse = (reqBody) => {	
	let newCourse =  new Course ({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})
	return newCourse.save().then((course, error) => {
		if(error) {
			return true
		} else {
			return course
		}
	})
}


// module.exports.getProfile = (data) => {
// 	return user.findById(data.userId).then(result => {
// 		result.password = ''
// 		return result		
// 	})
// }